# How to run this project ?
## Installing dependencies
```yarn install ```
## Running the app 
```yarn start```
# No yarn installed ?
 Refer this [Website](https://yarnpkg.com/lang/en/docs/install/) to install yarn.
