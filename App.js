import React,{Component} from 'react';
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import HomePage from './pages/HomePage';
import NewsDetails from './pages/NewsDetailPage'
class App extends Component {
render(){return(<Navigator/>)}
}

const Navigator= createStackNavigator({
  Home:{screen:HomePage},
  Details:{screen:NewsDetails}
});

export default createAppContainer(Navigator);
