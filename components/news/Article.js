import React,{Component} from 'react'
import {  Text, View,TouchableHighlight } from 'react-native';

export default function Article(props) {
    const {navigation,article,separators} = props;
    return(
        <TouchableHighlight
            onPress={()=>{navigation.navigate('Details',{news:article});
            }}
            onShowUnderlay={separators.highlight}
            onHideUnderlay={separators.unhighlight}
            >
              <View>
              <Text>
                {article.title}
              </Text>
              </View>
            </TouchableHighlight>
    )
}