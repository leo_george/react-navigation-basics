import React,{Component} from 'react'
import { StyleSheet,View,FlatList,AsyncStorage } from 'react-native';
import Article from '../components/news/Article';
export default class HomePage extends Component{
    constructor(props){
        super(props);
        this.state={
          data:[]
        }
        this._getNews=this._getNews.bind(this);
        this._setCache=this._setCache.bind(this);
        this._getCache=this._getCache.bind(this);
      }
     
     _getNews= async () => {
       const res= await fetch('https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=1d689c49c8ad4a7798f9cf055ff2802c');
       return await res.json();
     };
     _getCache= async ()=>{
      try {
        const data= await AsyncStorage.getItem('@NewsApp:8743')
        if (data != null) {
          console.log(data);
          
        }
      } catch (error) {
        console.log('Error in _getCache');
        
      }
     }
     _setCache= async ()=>{
      try {
        await AsyncStorage.setItem('@NewsApp:8743',JSON.stringify({data:this.state.data})) 
      } catch (error) {
        console.log('error in _setCache');
        
      }
     }
     componentDidMount(){
       this._getNews()
       .then((res)=>{
         this.setState({data:res.articles});
         this._setCache(); 
       })
       .catch((error)=>{
        this._getCache()
       })
     }
      render(){
        return (
          <View style={styles.container}>
            <FlatList
              data={this.state.data}
              keyExtractor={(item,index)=>index.toString()}
              renderItem={({item,index,separators})=>(
                <Article navigation={this.props.navigation} article={item} separators ={separators}/>
              )}
            />
          </View>
        );
      }
      
    }
    const styles = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center',
        },
      });
        